
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");
// console.log("alpha");

// Function Declaration
//function stament defines a function with the specified
function printName(){
    console.log("alpha");
    console.log("alpha");
    console.log("alpha");
    console.log("alpha");
    console.log("alpha");
}
printName()
printName()
printName()
printName()
printName()


//Function declaration
function declarationFunction() {
    console.log("hello world")
};

declarationFunction()

//Function expression

let variableFunction =  function() {
    console.log("hello world expression")
};

variableFunction();

let functionExpression = function functionName(){
    console.log("hello world 2")
}

functionExpression();

declarationFunction = function(){
    console.log("update declarationFunction")
}

declarationFunction()

functionExpression = function(){
    console.log("updated FunctionExpression");
}
functionExpression()

const ConstFunction = function(){
    console.log("cannot be reassign")
}

ConstFunction()
//Function scoping 

 {
    let localVar = "Lopez"
 }

 let globalVar = "Smith"

 console.log(globalVar);
//  console.log(localVar);  error

function showNames(){
    const fuctionConst = "alpha";
    let functionLet = "joy";

    console.log(fuctionConst);
    console.log(functionLet);
}

showNames()

// console.log(fuctionConst);
// console.log(functionLet);


// Nested functions

function myNewFunction(){ 
    let name = "alpha1";
    function nestedFunction(){
        let nestedName = "joy2";
        console.log(name);
    }

    nestedFunction()
    // console.log(nestedName) error
    // console.log(name); allow
}

myNewFunction()
// nestedFunction() error


// Funtion and Global scope variable

let globalName = "cardo";

function myNewFunction2(){
    let nameInside = "Mario"
    console.log(globalName);
}

// myNewFunction2()


//  alert()
// alert("hello")

function showSampleAlert(){
    alert("hello Welcome to B248 Class");
}

// showSampleAlert()

// console.log("A will only ")


// promt()
//the inpunt from the prompt will be return a STRING

let samplePrompt = prompt("Enter your name");
console.log("hello " + samplePrompt);


let sampleNullPrompt = prompt( "Don't enter anything");

console.log(sampleNullPrompt);//null

function printWelcomeMessage(){
    let firstName = prompt("Enter you name")
    let lastName = prompt("Enter you name")

    console.log("hello" + firstName + " " + lastName)
}

printWelcomeMessage()


//Good Function Naming converntion

function getCourses(){
    let courses = ["Sci", "Eng" , "Math"]
    console.log(courses);
}

// Bad Function naming convention
getCourses()

function get(){
    let name = "Cardo dalisay";
    console.log(name);
}

get();

//Name your fuction in small caps. follow camelCase

//function displayCarInfo(){}