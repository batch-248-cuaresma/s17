/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function aboutUser() {
		let name = window.prompt("What is your name?");
		let age = window.prompt("How old are you?");
		let location = window.prompt("Where do you live?");
	   console.log("Hello, " + name);
	   console.log("Your are " + age + " years old.")
	   console.log("You live in " + location);
		
	   }
	   
	   aboutUser()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function userTopFiveBands() {
		let band1 = window.prompt("What is your top  1 favorite band?");
		let band2 = window.prompt("What is your top  2 favorite band?");
		let band3 = window.prompt("What is your top  3 favorite band?");
		let band4 = window.prompt("What is your top  3 favorite band?");
		let band5 = window.prompt("What is your top  5 favorite band?");
	   console.log("1. " + band1);
	   console.log("2. " + band2);
	   console.log("3. " + band3);
	   console.log("4. " + band4);
	   console.log("5. " + band5);
		
	   }
	   
	   userTopFiveBands()
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/	
	
//A BEAUTIFUL MIND
//FIGHT CLUB
//THE LOVELY BONES
//Harry Potter and the Sorcerer's Stone 
//THE CHRONICLES OF NARNIA: THE LION, THE WITCH AND THE WARDROBE

	//third function here:
	function userTop5Movies() {
		let movie1 = window.prompt("What is your top  1 favorite Movie?");
		let movie2 = window.prompt("What is your top  2 favorite Movie?");
		let movie3 = window.prompt("What is your top  3 favorite Movie?");
		let movie4 = window.prompt("What is your top  4 favorite Movie?");
		let movie5 = window.prompt("What is your top  5 favorite Movie?");
	   
		console.log("1. " + movie1);
		console.log("Rotten tomatoes Ratind: 93%")
	   console.log("2. " + movie2);
	   console.log("Rotten tomatoes Ratind: 96%")
	   console.log("3. " + movie3);
	   console.log("Rotten tomatoes Ratind: 31%")
	   console.log("4. " + movie4);
	   console.log("Rotten tomatoes Ratind: 81%")
	   console.log("5. " + movie5);
	   console.log("Rotten tomatoes Ratind: 75%")
		
	   }
	   
	   userTop5Movies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();